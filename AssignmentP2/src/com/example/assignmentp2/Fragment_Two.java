package com.example.assignmentp2;

import java.util.ArrayList;

import com.example.assignmentp2.Fragment_First.OnButtonClickedListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;



public class Fragment_Two extends Fragment {

	
	View v;
	public String name;
	public String age;
	public String food;
	static public int i = 0;
	EditText etName;
	EditText etAge;
	Spinner spinner;
	Button Add;
	Button Done;
	ArrayList<String> customerData = new ArrayList<String>();
	OnDoneButtonClickedListener mCallback;
	
	public interface OnDoneButtonClickedListener {
	    
		 public void onDoneButtonSelected(ArrayList<String> arryList);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.fragment_two, container, false);
		spinner = (Spinner) v.findViewById(R.id.spinner1);
		etName = (EditText) v.findViewById(R.id.editText1);
		etAge = (EditText) v.findViewById(R.id.editText2);
		Add = (Button) v.findViewById(R.id.bAdd);
		Done = (Button) v.findViewById(R.id.bDone);
		Add.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				name = etName.getText().toString();
				age = etAge.getText().toString();
				food = spinner.getSelectedItem().toString();
				if (!((name.equals("")) && (age.equals("")))){
				customerData.add(name);
				customerData.add(age);
				customerData.add(food);
				etName.setText(null);
				etAge.setText(null);
				}
			}
		});

		Done.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(customerData.size()>0)
				mCallback.onDoneButtonSelected(customerData);
				
			}
		});
		String [] values = 
	        {"Pasta","Pizza","Nachos","Roti","dumplings","Pastery"};
	    Spinner spinner = (Spinner) v.findViewById(R.id.spinner1);
	    ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
	    LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
	    spinner.setAdapter(LTRadapter);
		return v;
	}
	
	@Override
	 public void onAttach(Activity activity) {
	    super.onAttach(activity);
	       
	    try {
	         mCallback = (OnDoneButtonClickedListener) activity;
	    	} catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString()
	          + " must implement OnHeadlineSelectedListener");
	    	}
	    }

}
