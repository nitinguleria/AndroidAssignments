package com.example.assignmentp2;

	

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class Fragment_First extends Fragment {

	 View v;
	 Button btEnterName;
	 Button btLoad;
	 Button btStore;
	 Button btView;
	 Button btExit;
	 OnButtonClickedListener mCallback;
	 
	 public interface OnButtonClickedListener {
	        
		 public void onButtonSelected(String buttonName);
	 }
	 
	 @Override
     public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState){
		v=inflater.inflate(R.layout.fragment_first, container, false);
		btEnterName=(Button)  v.findViewById(R.id.btEnter);
		btLoad=(Button) v.findViewById(R.id.btLoad);
		btView=(Button) v.findViewById(R.id.btView);
		btStore=(Button) v.findViewById(R.id.btStore);
		btExit=(Button) v.findViewById(R.id.btExit);
		btEnterName.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onButtonSelected("btEnterName");
				
			}
		});
		btLoad.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onButtonSelected("btLoad");
				
			}
		});
		btView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onButtonSelected("btView");
				
			}
		});
		btStore.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onButtonSelected("btStore");
				
			}
		});btExit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mCallback.onButtonSelected("btExit");
				
			}
		});
		return v;		
	   }
	 
	 @Override
	 public void onAttach(Activity activity) {
	    super.onAttach(activity);
	       
	    try {
	         mCallback = (OnButtonClickedListener) activity;
	    	} catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString()
	          + " must implement OnHeadlineSelectedListener");
	    	}
	    }
	
	    
	    
	
	   
	  
}
