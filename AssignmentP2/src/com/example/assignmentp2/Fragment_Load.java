package com.example.assignmentp2;

import java.util.ArrayList;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Fragment_Load extends ListFragment {
	ArrayList<String> bValue = new ArrayList<String>();
	OnLoadClickedListener mCallback;

	public interface OnLoadClickedListener {

		public void onLoadSelected(String loadName);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		bValue = this.getArguments().getStringArrayList("loadData");
		 
        if(!bValue.isEmpty()){
        	
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_1, bValue);
		setListAdapter(adapter);
	}
	}
	
	 @Override
	 public void onAttach(Activity activity) {
	    super.onAttach(activity);
	       
	    try {
	         mCallback = (OnLoadClickedListener) activity;
	    	} catch (ClassCastException e) {
	    	throw new ClassCastException(activity.toString()
	          + " must implement OnHeadlineSelectedListener");
	    	}
	    }

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// do something with the data
		String myLong=bValue.get((int) id);
		if (mCallback != null) {
			mCallback.onLoadSelected(myLong);
			Toast toast = Toast.makeText(getActivity(), "File "+ myLong+" loaded", 3);
	   		 toast.show();
		}
	}

}