package com.example.assignmentp4;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public  class DummyFragment extends Fragment {
	/**
	 * The fragment argument representing the section number for this
	 * fragment.
	 */
	public static final String ARG_SECTION_NUMBER = "section_number";

	public DummyFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_adapter,
				container, false);
		Bundle b=getArguments();
		String[] names=b.getStringArray("key");
		String url="https://www.google.ca/search?q="+names[b.getInt("pos")];
		String name=b.getString("name");
		WebView browser =(WebView)  rootView.findViewById(R.id.webView1);
		
		  browser.getSettings().setLoadsImagesAutomatically(true);
	      browser.getSettings().setJavaScriptEnabled(true);
	      browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	      
		browser.setWebViewClient(new MyBrowser());
		
		browser.loadUrl(url	);
		
		return rootView;
	}
	
	private class MyBrowser extends WebViewClient {
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
		
		
	}
}