package com.example.assignmentp4;



import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

public class MultipleTabs extends FragmentActivity {

	SectionsPagerAdapter mSectionsPagerAdapter;
	
	ViewPager mViewPager;
	
	int arrayLength;
	String[] array;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multipletabs);

		 array = getIntent().getExtras().getStringArray("key");
		arrayLength= array.length;
		
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());
		mSectionsPagerAdapter.SetArguements(array);
		
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

	}
	
	
	
}
