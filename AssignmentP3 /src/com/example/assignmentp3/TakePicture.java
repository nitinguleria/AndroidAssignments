package com.example.assignmentp3;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class TakePicture extends Activity implements SurfaceHolder.Callback{

	int cameraIDb = CameraInfo.CAMERA_FACING_BACK;
	int cameraIDf = CameraInfo.CAMERA_FACING_FRONT;

	private ImageView iv_image;
	private SurfaceView sv;
    private Bitmap bmp;
    private SurfaceHolder sHolder; 
    private Camera mCamera;
    private Parameters parameters;
    String currentLatitude, currentLongitude;
    Camera.PictureCallback mCall;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Remove titlebar within app
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    		WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_take_picture);
		

        
        iv_image = (ImageView) findViewById(R.id.imageView1);
        sv = (SurfaceView) findViewById(R.id.surfaceView);
        sHolder = sv.getHolder();
        sHolder.addCallback(this);
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    
       
        
        Bundle bundle = this.getIntent().getExtras();
        currentLatitude = bundle.getString("cLatitude");
        currentLongitude = bundle.getString("cLongitude");
        
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Status");
		alertDialog.setMessage("GPS Location and Picture have been saved! Tap on picture to go back to main screen.");
		
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alertDialog.show();
	}
	
    //Camera Functions
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3)
    {
        //get camera parameters
        parameters = mCamera.getParameters();
        setCameraDisplayOrientation(TakePicture.this, cameraIDb, mCamera);
        //set camera parameters
        final int rotation1 = ((WindowManager) TakePicture.this.getSystemService(Context.WINDOW_SERVICE)).
        		getDefaultDisplay().getOrientation();
        if(rotation1 == Surface.ROTATION_90 || rotation1 == Surface.ROTATION_270)
        {
            parameters.setPreviewSize(sv.getWidth(), sv.getHeight());                           
        }
        mCamera.setParameters(parameters);
        
        mCamera.startPreview();
        
        //sets what code should be executed after the picture is taken
        mCall = new Camera.PictureCallback()
        {
        	public void onPictureTaken(byte[] data, Camera camera)
            {
        		//decode the data obtained by the camera into a Bitmap
                //bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                //set the iv_image
                //iv_image.setImageBitmap(bmp);
                FileOutputStream outStream = null;
                if (currentLatitude != null && currentLongitude != null) {
                	try{
                		outStream = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/appP3/(" 
                				+ currentLatitude + ")(" + currentLongitude + ").JPG");
                		outStream.write(data);
                		outStream.close();
                		bmp = loadImage(Environment.getExternalStorageDirectory().getPath() + "/appP3/(" 
                				+ currentLatitude + ")(" + currentLongitude + ").JPG");
                        if (bmp.getWidth() > bmp.getHeight()) {
                            Matrix matrix = new Matrix();
                            //Image should appear in the orientation that it was taken
                            final int rotation = ((WindowManager) TakePicture.this.getSystemService(Context.WINDOW_SERVICE)).
                            		getDefaultDisplay().getOrientation();
                            if(rotation == 0)           matrix.postRotate(90);
                            else if (rotation == 1)    	matrix.postRotate(0);
                            else if (rotation == 2)   	matrix.postRotate(90);
                            else /*(rotation == 3)*/	matrix.postRotate(180);
                            //matrix.postScale(-1,1);
                            bmp = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), 
                            		bmp.getHeight(), matrix, true);
                        }
                		iv_image.setImageBitmap(bmp);
                	} catch (FileNotFoundException e) {
                		Log.d("CAMERA", e.getMessage());
                	} catch (IOException e) {
                		Log.d("CAMERA", e.getMessage());
                	}
                }
            }
        };
        mCamera.takePicture(null, null, mCall);
    }
    
    public void surfaceCreated(SurfaceHolder holder)
    {
    	// The Surface has been created, acquire the camera and tell it where
    	// to draw the preview.
    	if(Camera.getNumberOfCameras() == 0) {
    		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
    		alertDialog.setTitle("Device Status");
    		alertDialog.setMessage("There are no cameras on this device.");
    		
    		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				dialog.cancel();
    			}
    		});
    		alertDialog.show();    	
    	}
    	else {
    		//If there are cameras on the device, then cameraID == 0 always exists
    		//this applies to devices with either one camera or two cameras
    		mCamera = Camera.open(cameraIDb);
    		try {
    			mCamera.setPreviewDisplay(holder);
         
    		} catch (IOException exception) {
    			mCamera.release();
    		    mCamera = null;
    		}
    	}
    }
    
    public void surfaceDestroyed(SurfaceHolder holder)
    {
    	//stop the preview
        mCamera.stopPreview();
        //release the camera
        mCamera.release();
        //unbind the camera from this object
        mCamera = null;
    }
    
    public static void setCameraDisplayOrientation(Activity activity,
            int cameraId, android.hardware.Camera camera) {
    	android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }
        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else { 
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }
    
	private Bitmap loadImage(String imgPath) {
	    BitmapFactory.Options options;
	    try {
	        options = new BitmapFactory.Options();
	        options.inSampleSize = 2;
	        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
	        return bitmap;
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public void backtomain(View view) {
		onBackPressed();
	}
}
