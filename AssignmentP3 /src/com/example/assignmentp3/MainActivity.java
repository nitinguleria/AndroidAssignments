package com.example.assignmentp3;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends Activity implements SensorEventListener {
	
	private SensorManager sensorManager;
	TextView view;
	private long lastUpdate;
	Button GPS_button;
	GPSTracker gps;
	double latitude, longitude;
	boolean stopShake = true;
	boolean startTimer = false;

	String DBNAME = "gpsDB";
	String TABLENAME = "gpsTable";
	SQLiteDatabase sampleDB;
	long stopShakeTime;
	long actualTime;
	long newTime;
	Vibrator v;
	//long pattern[] = {0, 100, 100, 100, 100, 100, 100, 100, 100};
	String currentLatitude, currentLongitude;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		//Remove titlebar within app
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
	    
	    //locate widgets
	    v = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);
	    view = (TextView) findViewById(R.id.textView);
	    GPS_button = (Button)findViewById(R.id.button1);
	    //int cameras = Camera.getNumberOfCameras();
	    File f = new File(Environment.getExternalStorageDirectory().getPath() + "/appP3/");
	    f.mkdir();

	    sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	    lastUpdate = System.currentTimeMillis();
	    
	    final Animation a = AnimationUtils.loadAnimation(this, R.anim.translate);
	    a.setRepeatMode(Animation.RESTART);
	    a.setRepeatCount(Animation.INFINITE);
	    view.startAnimation(a);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			getAccelerometer(event);
		}
	}

	private void getAccelerometer(SensorEvent event) {
		float[] values = event.values;
		float x = values[0];
		float y = values[1];
		float z = values[2];

		//calculate total acceleration 
		float accelerationSquareRoot = (x*x + y*y + z*z)/(SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);

		
		//if acceleration calculated is abnormal(more than 2 times gravity)
		if (accelerationSquareRoot >= 2 && stopShake) 
		{
			stopShake = false;
			actualTime = System.currentTimeMillis();
			//Delay between stores due to shakes(to prevent a crazy amount of stores on a prolonged shake)
			if (actualTime - lastUpdate < 1000) {
				return;
			}
		  
			lastUpdate = actualTime;
			Toast.makeText(this, "Device has been shaken.", Toast.LENGTH_SHORT).show();
			v.vibrate(500);
			//v.vibrate(pattern, -1);
			
			gps = new GPSTracker(MainActivity.this);
			
			if(gps.canGetLocation()) {
				latitude = gps.getLatitude();
				longitude = gps.getLongitude();
				//set textviews
				boolean northSouth = true;
				if (latitude < 0) {
					northSouth = false;
					latitude = latitude * -1;
				}
				boolean eastWest = true;
				if (longitude < 0) {
					eastWest = false;
					longitude = longitude * -1;
				}
				currentLatitude = Double.toString(latitude);
				if (northSouth) currentLatitude+="N";
				else currentLatitude+="S";
				currentLongitude = Double.toString(longitude);
				if (eastWest) currentLongitude+="E";
				else currentLongitude+="W";
				//store coordinates into database
			    sampleDB = this.openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
		        sampleDB.execSQL("CREATE TABLE IF NOT EXISTS " + TABLENAME + " (Latitude VARCHAR, Longitude VARCHAR, " +
		        		" Filename VARCHAR);");
		        //check if entry exists
                Cursor c = null;
                c = sampleDB.rawQuery("SELECT * FROM " + TABLENAME + " WHERE Filename = '(" + currentLatitude + ")(" + currentLongitude + ").JPG'", null);
                //if entry exists, notify user of overwrite
                if (c!=null && c.moveToFirst()) {
            		AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            		alertDialog.setTitle("GPS Overwrite");
            		alertDialog.setMessage("Note that there has been a previous entry at the current GPS coordinates. Image file will be overwritten.");
            		
            		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            			public void onClick(DialogInterface dialog, int which) {
            				dialog.cancel();
            			}
            		});
            		alertDialog.show();    	
                }
                //otherwise create a new entry
                else {
                	sampleDB.execSQL("INSERT INTO " + TABLENAME + " Values ('" + currentLatitude + "', '" +
                			currentLongitude + "', '(" + currentLatitude + ")(" + currentLongitude + ").JPG');");
                }
                sampleDB.close();
		        
			}
			else {
				gps.showSettingsAlert();
			}
		}
		//if acceleration is equal to that of gravity + some leeway to take into account minor accelerometer movement
		else if (accelerationSquareRoot <= 1.1) { 
			//if shaking just stopped
			if(!startTimer && !stopShake) {
				actualTime = System.currentTimeMillis();
				startTimer = true;
			}
		    //Check current time
			stopShakeTime = System.currentTimeMillis();
			//if shaking has stopped for at least 1 second, capture image
			if (startTimer && !stopShake && ((stopShakeTime - actualTime) >= 1000)) {
				//wait 1 second after shaking stops
				//long 
				Bundle bundle = new Bundle();
				bundle.putString("cLatitude", currentLatitude);
				bundle.putString("cLongitude", currentLongitude);
		    	Intent intent = new Intent(this, TakePicture.class);
		    	intent.putExtras(bundle);
		    	startActivity(intent);
				stopShake = true;
				startTimer = false;
			}
			return;
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		//do nothing
  }

	@Override
	protected void onResume() {
		super.onResume();
		// register the sensor once app is re-entered
		sensorManager.registerListener(this,
        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
        SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	protected void onPause() {
		// turn off the sensor listener when app is interrupted
		super.onPause();
		sensorManager.unregisterListener(this);
	}
	
    //Called when list items button is clicked
    public void listItems(View view) {
    	//Respond to button click, making sure there is at least one saved entry into database
	    sampleDB = this.openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
        sampleDB.execSQL("CREATE TABLE IF NOT EXISTS " + TABLENAME + " (Latitude VARCHAR, Longitude VARCHAR, " +
        		" Filename VARCHAR);");
        Cursor c = sampleDB.rawQuery("SELECT Latitude, Longitude FROM " + TABLENAME, null);
        if(c!=null) {
        	if(c.moveToFirst()){
        		//if table is not empty...
            	Intent intent = new Intent(this, ListItemsActivity.class);
            	startActivity(intent);
        	}
            else {
            	//Display text notifying that there are no list items
        		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        		alertDialog.setTitle("List Status");
        		alertDialog.setMessage("There are no items saved in list.");
        		
        		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        			public void onClick(DialogInterface dialog, int which) {
        				dialog.cancel();
        			}
        		});
        		alertDialog.show();    	
            }
        }
    }
}