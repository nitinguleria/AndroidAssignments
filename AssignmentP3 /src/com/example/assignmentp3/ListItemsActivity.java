package com.example.assignmentp3;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ListItemsActivity extends Activity {
	
	ListView lv;
	TextView tv;
	SQLiteDatabase myDB;
	String DBNAME = "gpsDB";
	String TABLENAME = "gpsTable";
	String item;
	Cursor c;
	ArrayAdapter<String> aa;
	Button button;
	int counter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Remove titlebar within app
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_items);
		
		//locate widget
		button = (Button)findViewById(R.id.button1);
		
		//retrieve saved list
		myDB = this.openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
        c = myDB.rawQuery("SELECT Latitude, Longitude FROM " + TABLENAME, null);
        counter = 0;
        if (c!=null) {
        	if(c.moveToFirst()) {
        		do {
        			counter++;
        		}while(c.moveToNext());
        	}
		}
		String list[] = new String[counter];
		c = myDB.rawQuery("SELECT Latitude, Longitude FROM " + TABLENAME, null);
		int i = 0;
        if (c!=null) {
        	if(c.moveToFirst()) {
        		do {
        			list[i] = "(";
        			list[i] += c.getString(c.getColumnIndex("Latitude"));
        			list[i] +=")(";
        			list[i] += c.getString(c.getColumnIndex("Longitude"));
        			list[i] +=")";
        			i++;
        		}while(c.moveToNext());
        	} 
		}		
        myDB.close();
		lv = (ListView)findViewById(R.id.listview1);
		tv = (TextView)findViewById(R.id.textview1);
		
		//populate list with gps coordinates
		aa = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
		lv.setAdapter(aa);
		lv.setClickable(true);
		lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			 @Override
			 public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				    Object o = lv.getItemAtPosition(position);
				    item = o.toString();
				//When an item is selected, give option to delete or open
					AlertDialog.Builder alertDialog = new AlertDialog.Builder(ListItemsActivity.this);
					alertDialog.setTitle("List Item Options");

					//if camera exists, give option to open the image
					if (Camera.getNumberOfCameras() > 0) {
						alertDialog.setMessage("Would you like to open or delete the selected item?");
						alertDialog.setPositiveButton("Open", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.cancel();
								String fName = Environment.getExternalStorageDirectory().getPath();
								fName += "/appP3/";
								fName += item;
								fName += ".JPG";
								//send to image view
								Bundle bundle = new Bundle();
								bundle.putString("path", fName);
								Intent intent = new Intent(ListItemsActivity.this, ShowPicture.class);
								intent.putExtras(bundle);
								startActivity(intent);
							}
						});
					}
					//otherwise just give the option to delete
					else alertDialog.setMessage("Would you like to delete the selected item?");
					alertDialog.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
					        //Double check to see if delete is desired
					    	AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(ListItemsActivity.this);
					    	alertDialog2.setTitle("Confirm Delete");
					    	alertDialog2.setMessage("Are you sure you want to delete this GPS entry and it's photograph?");
					    		
					    	alertDialog2.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
					    		public void onClick(DialogInterface dialog, int which) {
					    			dialog.cancel();
					    		}
					    	});
							alertDialog2.setNegativeButton("Confirm", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int which) {
									dialog.cancel();
									myDB = ListItemsActivity.this.openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
									myDB.execSQL("DELETE FROM " + TABLENAME + " WHERE Filename = '" + item + ".JPG';");
									myDB.close();
									String fName = Environment.getExternalStorageDirectory().getPath();
									fName += "/appP3/";
									fName += item;
									fName += ".JPG";
									File tempFile = new File(fName);
									boolean deleted = tempFile.delete();
									onBackPressed();								
								}
							});
					    	alertDialog2.show();    	
						}
					});
					alertDialog.show();
			  }
		});
	}
	
	public void backtomain(View view) {
		onBackPressed();
	}
	
	public void deleteAll(View view) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Confirm Delete");
		alertDialog.setMessage("Are you sure you want to delete all entires?");
		
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				//open database and select all filenames
				myDB = ListItemsActivity.this.openOrCreateDatabase(DBNAME, MODE_PRIVATE, null);
				c = myDB.rawQuery("SELECT Filename FROM " + TABLENAME, null);
				//instantiate variables
				int deleteCount = 0;
				int i = 0;
				String deleteFilename[] = new String[counter];
				//store all filenames
				if (c!=null) {
					if(c.moveToFirst()) {
						do {
							deleteFilename[deleteCount] = c.getString(c.getColumnIndex("Filename"));
							deleteCount++;
						}while(c.moveToNext());
					}
				}
				//delete all rows from table
				myDB.execSQL("DELETE FROM " + TABLENAME + ";");
				//delete all files(images)
				do {
					String fName = Environment.getExternalStorageDirectory().getPath();
					fName += "/appP3/";
					fName += deleteFilename[i];
					File tempFile = new File(fName);
					boolean deleted = tempFile.delete();
					i++;
				} while (i < deleteCount);
				//inform user of delete
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(ListItemsActivity.this);
				alertDialog.setTitle("Deletion Complete");
				alertDialog.setMessage("All list items and files have been deleted.");
		
				alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						onBackPressed();
					}
				});
				alertDialog.show();
			}
		});
		alertDialog.show(); 
	}
}
