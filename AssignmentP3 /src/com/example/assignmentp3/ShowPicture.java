package com.example.assignmentp3;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class ShowPicture extends Activity {

	ImageView iv;
	Bitmap bmp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Remove titlebar within app
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	        WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_picture);
		
        Bundle bundle = this.getIntent().getExtras();
        String filePath = bundle.getString("path");
        
        iv = (ImageView)findViewById(R.id.imageView);
        bmp = loadImage(filePath);

        if (bmp.getWidth() > bmp.getHeight()) {
            Matrix matrix = new Matrix();
            //Image should appear in the orientation that it was taken
            final int rotation = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).
            		getDefaultDisplay().getOrientation();
            if(rotation == 0)           matrix.postRotate(90);
            else if (rotation == 1)    	matrix.postRotate(0);
            else if (rotation == 2)   	matrix.postRotate(90);
            else /*(rotation == 3)*/	matrix.postRotate(180);
            //matrix.postScale(-1,1);
            bmp = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), 
            		bmp.getHeight(), matrix, true);
        }
        
        iv.setImageBitmap(bmp);

	}
	
	private Bitmap loadImage(String imgPath) {
	    BitmapFactory.Options options;
	    try {
	        options = new BitmapFactory.Options();
	        options.inSampleSize = 2;
	        Bitmap bitmap = BitmapFactory.decodeFile(imgPath, options);
	        return bitmap;
	    } catch(Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	public void backtolist(View view) {
		onBackPressed();
	}
}
