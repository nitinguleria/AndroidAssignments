package com.example.assignment1;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

	int tvClick = 0;
	int ivClick = 0;
	Button button1;
	Button button2;
	TextView tView1;
	ImageView iView1;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    button1 = (Button) findViewById(R.id.button1);
    button2 = (Button) findViewById(R.id.button2);
    tView1= (TextView) findViewById(R.id.textView1);
    iView1=(ImageView) findViewById(R.id.imageView1);
   
    	
   	button1.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
        
			
			tView1.setText("The view has been clicked " +String.valueOf(tvClick+1)+" times.");
			tvClick = tvClick+1;
			
		}
	});
   	
   	button2.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(ivClick % 2==0)
			iView1.setVisibility(v.INVISIBLE);
		    else
		    iView1.setVisibility(v.VISIBLE);
			ivClick = ivClick+1;
		
		}
   	}
	);
   	
   	
			
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings  :
                tView1.setText("Things haven't started yet");
                tvClick=0;
                return true; 
            default:
                return super.onOptionsItemSelected(item);
    
      }

    }
}